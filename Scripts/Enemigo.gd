extends Sprite

const noL = [1, 3, 5, 9, 13, 15, 16]
const noR = [1, 4, 6, 10, 14, 15, 16]
const noU = [2, 3, 4, 11, 13, 14, 15]
const noD = [2, 5, 6, 12, 13, 14, 16]

var health
var dano
var posicion = Vector2.ZERO

var debug = true

func initMonstruo(texture, position, type):
	set_texture(texture)
	visible = false
	posicion = position
	if type == 0:
		health = 20
		dano = 2
	elif type == 1:
		health = 30
		dano = 3
	elif type == 2:
		health = 40
		dano = 2
	else:
		health = 50
		dano = 3

func move(map, posJugador):
	if debug:
		print("Enemigo en: ",posicion)
		
	var izq = true
	var der = true
	var down = true
	var up = true
	#Ver para donde moverse y ver si se puede pasar
	var pos = map[posicion.x][posicion.y]
	
	if pos in noL:
		izq = false
	if pos in noR:
		der = false
	if pos in noU:
		up = false
	if pos in noD:
		down = false
	
	#Revisar proximidad con el jugador
	if izq:
		if posJugador == Vector2(posicion.x, posicion.y - 1):
			return hacerDano()
	if der:
		if posJugador == Vector2(posicion.x, posicion.y + 1):
			return hacerDano()
	if up:
		if posJugador == Vector2(posicion.x - 1, posicion.y):
			return hacerDano()
	if down:
		if posJugador == Vector2(posicion.x + 1, posicion.y):
			return hacerDano()
			
	#que se hará, 0 quedarse quieto, 1 moverse
	var mover = randi() % 2
	if mover != 0:
		while true:
			var eleccion = randi () % 5
			var new = Vector2.ZERO
			var next = false
			if izq and eleccion == 0:
				new = Vector2(posicion.x, posicion.y - 1)
			elif der and eleccion == 1:
				new = Vector2(posicion.x, posicion.y + 1)
			elif up and eleccion == 2:
				new = Vector2(posicion.x - 1, posicion.y)
			elif down and eleccion == 4:
				new = Vector2(posicion.x + 1, posicion.y)
			else:
				continue
				
			if posJugador == new:
				continue
			for mon in get_tree().get_nodes_in_group("Monsters"):
				if new == mon.posicion:
					next = true
			if not next:
				posicion = new
				return 0
	else:
		return 0
func hacerDano():
	#% de fallo
	return randi() % 10 + dano

func recibir(cantidad):
	health -= cantidad
	if health <= 0:
		queue_free()
