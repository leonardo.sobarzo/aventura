extends TextureProgress
#http://kidscancode.org/godot_recipes/ui/unit_healthbar/

onready var green = preload("res://Assets/Imagenes/barHorizontal_green.png")
onready var yellow = preload("res://Assets/Imagenes/barHorizontal_yellow.png")
onready var red = preload("res://Assets/Imagenes/barHorizontal_red.png")

func updateHealth(value):
	set_progress_texture(green)
	if value < max_value * 0.5:
		set_progress_texture(yellow)
	if value < max_value * 0.25:
		set_progress_texture(red)
	set_value(value)
