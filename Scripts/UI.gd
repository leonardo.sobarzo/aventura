extends Control

const width = 10
const height = 10
const cantMapas = 2 #cambiar

#parte weona
const notN = [2, 3, 4, 11, 13, 14, 15]
const notO = [1, 3, 5, 9, 13, 15, 16]
const notS = [2, 5, 6, 12, 13, 14, 16]
const notE = [1, 4, 6, 10, 14, 15, 16]

#nodos
onready var hpBar = $HP
onready var mapa = $mapa
onready var flecha = $flecha
onready var vision = $vision
onready var monst = $Monstruos

#imagenes
onready var brujo = [preload("res://Assets/Bichos/brujo_1.png"), preload("res://Assets/Bichos/brujo_2.png"),
preload("res://Assets/Bichos/brujo_3.png"), preload("res://Assets/Bichos/brujo_4.png")]
onready var uveja = [preload("res://Assets/Bichos/uveja_1.png"), preload("res://Assets/Bichos/uveja_2.png"),
preload("res://Assets/Bichos/uveja_3.png"), preload("res://Assets/Bichos/uveja_4.png")]
onready var dino = [preload("res://Assets/Bichos/dino_1.png"), preload("res://Assets/Bichos/dino_2.png"),
preload("res://Assets/Bichos/dino_3.png"), preload("res://Assets/Bichos/dino_4.png")]
onready var jalea = [preload("res://Assets/Bichos/jaleo_1.png"), preload("res://Assets/Bichos/jaleo_2.png"),
preload("res://Assets/Bichos/jaleo_3.png"), preload("res://Assets/Bichos/jaleo_4.png")]

#escenas
onready var enemigo = preload("res://Escenas/Enemigo.tscn")

var map = []
var tabuList = []
var nivel = 0

#Player
enum DIR {N, O, S, E}
var health = 100
var posicion = Vector2.ZERO
var direccion = DIR.N
var ataque

var debug = false


func _ready():
	randomize()
	ataque = 10
	# Deberḉḉía haber una mejor manera
	for x in range(width):
		map.append([])
		map[x] = []
		for y in range(height):
			map[x].append([])
			map[x][y] = 0
	#Leer nivel
	leerArchivo()
	
	#Escoger lugar donde aparecer
	posicion = posicionInicial(true)
	verTile()
	ver()
	
	
	if debug:
		print("Posicion inicial: ", posicion)
		print("Direccion actual: ", direccion)
		print("------------------------")
	

func _process(_delta):
	if health <= 0:
		#Change stage
		gameover()
		
	if Input.is_action_just_pressed("ui_up"):
		if adelante():
			bajarVida(1)
		verTile()
		ver()
		if debug:
			print("Direccion actual: ", direccion)
			print("Posicion actual: ", posicion)
			print("Tipo de celda: ", map[posicion.x][posicion.y])
			
		for i in get_tree().get_nodes_in_group("Monsters"):
			bajarVida(i.move(map, posicion))
			
	if Input.is_action_just_pressed("ui_left"):
		cambiarDireccion(0)
		ver()
		if debug:
			print("Direccion actual: ", direccion)
	if Input.is_action_just_pressed("ui_right"):
		cambiarDireccion(1)
		ver()
		if debug:
			print("Direccion actual: ", direccion)
	if Input.is_action_just_pressed("ui_down"):
		cambiarDireccion(2)
		ver()
		if debug:
			print("Direccion actual: ", direccion)
	
	if Input.is_action_just_pressed("ui_accept"):
		gameover()
	

func leerArchivo():
	var file = File.new()
	var number = randi() % cantMapas + 1
	
	if mapa.get_children().size()  > 0:
		for n in mapa.get_children():
			mapa.remove_child(n)
			n.queue_free()
	
	while number in tabuList:
		number = randi() % cantMapas + 1
		
	var string = "res://Assets/Mapas/map_" + str(number) + ".txt"
#	var string = "res://Assets/Mapas/map_1.txt"

	nivel += 1
	$Textos/Lugar.text = "B-" + str(nivel)

	file.open(string, File.READ)
	for j in range(10):
		var linea = file.get_line()
		var numero = linea.split(" ")
		for i in range(10):
			map[j][i] = int(numero[i])
	file.close()
	
	if debug:
		print("Mapa: ", number)
	
	tabuList.append(number)
	
	initEnemigos(tabuList.size() + 1)


func verTile():
	var tile = Sprite.new()
	var string = "res://Assets/Tiles/tile_" + str(map[posicion.x][posicion.y]) + ".png"
	tile.centered = false
	tile.set_position(Vector2(6 + posicion.y * 27, 51 + posicion.x * 27))
	var texture = load(string)
	tile.texture = texture
	mapa.add_child(tile)


func posicionInicial(jugador):
	var x = 0
	var y = 0
	#Posición imposible de llegar
	while map[x][y] == 0:
		x = randi() % 10
		y = randi() % 10
	
	if jugador:
		health = 100
		flecha.set_position(Vector2(509.27, 108.534))
	
		flecha.set_position(Vector2(flecha.position.x + 19.5 + y * 27, flecha.position. y + 64.5 + x * 27))
		flecha.visible = true
		return Vector2(x,y)
	
	#Revisar si el jugador u otro monstruo tiene la casilla
	else:
		if Vector2(x,y) == posicion:
			return posicionInicial(false)
		
		for mon in get_tree().get_nodes_in_group("Monsters"):
			if Vector2(x, y) == mon.posicion:
				return posicionInicial(false)
		return Vector2(x, y)

#Fila columna, :|
func adelante():
	if direccion == DIR.N and not map[posicion.x][posicion.y] in notN:
		posicion = Vector2(posicion.x - 1, posicion.y)
		flecha.position = Vector2(flecha.position.x, flecha.position.y - 27)
		return true
	if direccion == DIR.O and not map[posicion.x][posicion.y] in notO:
		posicion = Vector2(posicion.x, posicion.y - 1)
		flecha.position = Vector2(flecha.position.x - 27, flecha.position.y)
		return true
	if direccion == DIR.S and not map[posicion.x][posicion.y] in notS:
		posicion = Vector2(posicion.x + 1, posicion.y)
		flecha.position = Vector2(flecha.position.x, flecha.position.y + 27)
		return true
	if direccion == DIR.E and not map[posicion.x][posicion.y] in notE:
		posicion = Vector2(posicion.x, posicion.y + 1)
		flecha.position = Vector2(flecha.position.x + 27, flecha.position.y)
		return true
	return false

#Detesto como quedo esto, las rotaciones de 0 es para evitar overflow
func cambiarDireccion(newDir):
	if newDir == 0:
		if direccion == DIR.N:
			direccion = DIR.O
			flecha.rotation_degrees -= 90
		elif direccion == DIR.O:
			direccion = DIR.S
			flecha.rotation_degrees -= 90
		elif direccion == DIR.S:
			direccion = DIR.E
			flecha.rotation_degrees -= 90
		elif direccion == DIR.E:
			direccion = DIR.N
			flecha.rotation_degrees = 0
	elif newDir == 1:
		if direccion == DIR.N:
			direccion = DIR.E
			flecha.rotation_degrees += 90
		elif direccion == DIR.E:
			direccion = DIR.S
			flecha.rotation_degrees += 90
		elif direccion == DIR.S:
			direccion = DIR.O
			flecha.rotation_degrees += 90
		elif direccion == DIR.O:
			direccion = DIR.N
			flecha.rotation_degrees = 0
	else:
		if direccion == DIR.N:
			direccion = DIR.S
			flecha.rotation_degrees += 180
		elif direccion == DIR.S:
			direccion = DIR.N
			flecha.rotation_degrees = 0
		elif direccion == DIR.O:
			direccion = DIR.E
			flecha.rotation_degrees += 180
		elif direccion == DIR.E:
			direccion = DIR.O
			flecha.rotation_degrees += 180

func bajarVida(cantidad):
	print (cantidad)
	health -= cantidad
	hpBar.updateHealth(health)

func ver():
	var queHay = 0
	if posicion.x == 9 or posicion.y == 9:
		#Mostrar muralla altiro
		return
		
	#TODO: esto solo muestra al primero, no si está más lejos
	if direccion == DIR.N:
		queHay = map[posicion.x - 1][posicion.y]
		for i in get_tree().get_nodes_in_group("Monsters"):
			if i.posicion.x == posicion.x - 1:
				i.visible = true
	elif direccion == DIR.O:
		queHay = map[posicion.x][posicion.y - 1]
		for i in get_tree().get_nodes_in_group("Monsters"):
			if i.posicion.y == posicion.y - 1:
				i.visible = true
	elif direccion == DIR.S:
		queHay = map[posicion.x + 1][posicion.y]
		for i in get_tree().get_nodes_in_group("Monsters"):
			if i.posicion.x == posicion.x + 1:
				i.visible = true
	else:
		queHay = map[posicion.x][posicion.y + 1]
		for i in get_tree().get_nodes_in_group("Monsters"):
			if i.posicion.y == posicion.y + 1:
				i.visible = true
		
	if debug:
		print("Que hay mas adelante: ", queHay)
		
	if queHay == 0:
		#Mostrar una muralla
		pass
	else:
		#Mostrar la cosa respectiva y monstruos
		pass

func gameover():
	leerArchivo()
	posicion = posicionInicial(true)
	verTile()

func revisarEnemigo():
	if direccion == DIR.N:
		for enemy in get_tree().get_nodes_in_group("Monsters"):
			if posicion.x == enemy.posicion.x - 1 and posicion.y == enemy.posicion.y:
				#Atacarlo
				enemy.recibir(ataque)
	elif direccion == DIR.O:
		for enemy in get_tree().get_nodes_in_group("Monsters"):
			if posicion.x == enemy.posicion.x and posicion.y == enemy.posicion.y - 1:
				#Atacarlo
				enemy.recibir(ataque)
	elif direccion == DIR.S:
		for enemy in get_tree().get_nodes_in_group("Monsters"):
			if posicion.x == enemy.posicion.x + 1 and posicion.y == enemy.posicion.y:
				#Atacarlo
				enemy.recibir(ataque)
	elif direccion == DIR.E:
		for enemy in get_tree().get_nodes_in_group("Monsters"):
			if posicion.x == enemy.posicion.x and posicion.y == enemy.posicion.y + 1:
				#Atacarlo
				enemy.recibir(ataque)

func initEnemigos(size):
	for _i in range(size):
		var monstruo = enemigo.instance()
		var tipo = randi() % 10
		var color = randi() % 3
		var especial = randi() % 100
		
		#color especial
		if especial <= 20:
			color = 3
			
		#jalea	
		if tipo < 4:
			monstruo.initMonstruo(jalea[color], posicionInicial(false), 0)
		#brujo
		elif tipo == 4 or tipo == 5:
			monstruo.initMonstruo(brujo[color], posicionInicial(false), 1)
		#uveja
		elif tipo == 6 or tipo == 6:
			monstruo.initMonstruo(uveja[color], posicionInicial(false), 2)
		#dino
		else:
			monstruo.initMonstruo(dino[color], posicionInicial(false), 3)
			
		monst.add_child(monstruo)
